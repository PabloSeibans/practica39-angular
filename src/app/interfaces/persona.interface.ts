export interface Person {
    email: string;
    tramits: number;
    description: string;
}