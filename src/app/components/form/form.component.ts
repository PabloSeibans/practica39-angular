import { Component, OnInit } from '@angular/core';
import { Person } from '../../interfaces/persona.interface';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  person: Person = {
    email: '',
    tramits: 0,
    description: ''
  };


  guardar():void{
    console.log(this.person.email);
    console.log(this.person.tramits);
    console.log(this.person.description);    
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(num: number): void{
    this.person.tramits = num;
  }

}
